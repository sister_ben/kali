/*************************************************************************
    > File Name: s1.c
    > Author: Lee_yellow
    > Mail: 1031208128@qq.com 
    > Created Time: 2021年11月27日 星期六 22时06分20秒
 ************************************************************************/
#include<stdio.h>
int main(){

	int ebp, esp;
	ebp = get_ebp();
	esp = get_esp();
	printf("ebp=%8x	esp=%8x\n", ebp, esp);
}
