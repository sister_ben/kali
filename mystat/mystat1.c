/*************************************************************************
    > File Name: mystat.c
    > Author: Lee_yellow
    > Mail: 1031208128@qq.com 
    > Created Time: 2021年11月04日 星期四 18时55分41秒
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include<time.h>
#include<dirent.h>
#include<direct.h>

struct stat *s;
struct statx *sx;
char buf;

int main(int argc,char **argv){
	s=(struct stat *)malloc(sizeof (struct stat));
	sx=(struct statx *)malloc(sizeof (struct statx));
	
	stat(argv[1],s);
	statx(argv[1],sx);
	printf("  FILE: %s\n",argv[1]);
	printf("  size: %-10ldblock: %-10ldIO Block: %-7ld",s->st_size,s->st_blocks,s->st_blksize);

	switch(s->st_mode & S_IFMT){
		case S_IFSOCK:	printf("socket\n");
						break;
		case S_IFLNK:	printf("symbolic link\n");
						break;
		case S_IFREG:	printf("regular file\n");
						break;
		case S_IFBLK:	printf("block device\n");
						break;
		case S_IFDIR:	printf("directory\n");
						break;
		case S_IFCHR:	printf("character device\n");
						break;
		case S_IFIFO:	printf("FIFO\n");
						break;
		default:		printf("bad file\n");
       	}
	printf("Device: %-10ldInode: %-10ldLinks: %ld\n",s->st_dev,s->st_ino,s->st_nlink);
	printf("Access: (");
	if(s->st_mode & S_IRUSR)	printf("r");
	else	printf("-");
	if(s->st_mode & S_IWUSR)	printf("w");
	else	printf("-");
	if(s->st_mode & S_IXUSR)	printf("x");
	else	printf("-");
	if(s->st_mode & S_IRGRP)	printf("r");
	else	printf("-");
	if(s->st_mode & S_IWGRP)	printf("w");
	else	printf("-");
	if(s->st_mode & S_IXGRP)	printf("x");
	else	printf("-");
	if(s->st_mode & S_IROTH)	printf("r");
	else	printf("-");
	if(s->st_mode & S_IWOTH)	printf("w");
	else	printf("-");
	if(s->st_mode & S_IXOTH)	printf("x");
	else	printf("-");
	printf(")\t");
	printf("Uid：%d\t Gid：%d\n",s->st_uid,s->st_gid);
	printf("Access: %s",ctime(&sx->stx_ctime));
	printf("Modify: %s",ctime(&sx->stx_atime));
	printf("Change: %s",ctime(&sx->stx_mtime));
	printf(" Birth: %s",ctime(&stat->stx_btime));
	return 0;
}
