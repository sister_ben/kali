/*************************************************************************
    > File Name: main.c
    > Author: Lee_yellow
    > Mail: 1031208128@qq.com 
    > Created Time: 2021年11月15日 星期一 11时18分32秒
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int sum(int N);

int main(int argc, char *argv[]) 
{
 int num, s; 
 num = atoi(argv[1]);
 s = sum(num);
 printf("The sum of my student number is : %d\n", s);
 return 0;
}
