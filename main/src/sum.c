/*************************************************************************
    > File Name: sum.c
    > Author: Lee_yellow
    > Mail: 1031208128@qq.com 
    > Created Time: 2021年11月15日 星期一 11时24分37秒
 ************************************************************************/
#include <stdio.h>
#include <stdlib.h>

int sum(int N)
{
 int S = 0;
 while (N != 0)
 {
  S += N % 10;
  N /= 10;
 }
 return S;
}
