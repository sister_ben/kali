#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "btree.h"

struct LinkQueue
{
    PNode f;
    PNode r;
};
typedef struct LinkQueue *PLinkQueue;

PLinkQueue CreatQ();
void AddQ(PLinkQueue plqu, PBinTreeNode x);
PBinTreeNode DeleteQ(PLinkQueue plqu);
int IsEmpty(PLinkQueue plqu);

#endif