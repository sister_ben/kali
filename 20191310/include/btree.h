#ifndef __BTREE_H__
#define __BTREE_H__


struct BinTreeNode;
typedef struct BinTreeNode *PBinTreeNode;
struct BinTreeNode
{
    char info;
    PBinTreeNode llink;
    PBinTreeNode rlink;
};//二叉树的节点

struct Node;
typedef struct Node *PNode;
struct Node
{
    PBinTreeNode info;
    PNode link;
};//入队根节点

PBinTreeNode createBinTree() ;
int LevelOrder(PBinTreeNode t);
#endif