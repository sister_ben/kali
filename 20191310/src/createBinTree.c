#include "btree.h"
#include <stdio.h>
#include<stdlib.h>

PBinTreeNode createBinTree() //树的建立（依照前序遍历）
{
    char ch;
    PBinTreeNode t;
    ch = getchar(); //输入二叉树数据
    if (ch == '#')  //判断二叉树是否为空
        t = NULL;
    else
    {
        t = (PBinTreeNode)malloc(sizeof(struct BinTreeNode)); //二叉树的生成
        t->info = ch;
        t->llink = createBinTree();
        t->rlink = createBinTree();
    }
    return t;
}