#include <stdio.h>
#include<stdlib.h>

struct BinTreeNode;
typedef struct BinTreeNode *PBinTreeNode;
struct BinTreeNode
{
    char info;
    PBinTreeNode llink;
    PBinTreeNode rlink;
};//二叉树的节点

struct Node;
typedef struct Node *PNode;
struct Node
{
    PBinTreeNode info;
    PNode link;
};//入队根节点

struct LinkQueue
{
    PNode f;
    PNode r;
};
typedef struct LinkQueue *PLinkQueue;

PBinTreeNode createBinTree() //树的建立（依照前序遍历）
{
    char ch;
    PBinTreeNode t;
    ch = getchar(); //输入二叉树数据
    if (ch == '#')  //判断二叉树是否为空
        t = NULL;
    else
    {
        t = (PBinTreeNode)malloc(sizeof(struct BinTreeNode)); //二叉树的生成
        t->info = ch;
        t->llink = createBinTree();
        t->rlink = createBinTree();
    }
    return t;
}

PLinkQueue CreatQ()
{
    PLinkQueue plqu = (PLinkQueue)malloc(sizeof(struct LinkQueue));
    if (plqu != NULL)
    {
        plqu->f = NULL;
        plqu->r = NULL;
    }
    else
        printf("Out of space!!\n");
    return plqu;
};

void AddQ(PLinkQueue plqu, PBinTreeNode x)
{
    PNode p;
    p = (PNode)malloc(sizeof(struct Node));
    if (p == NULL)
        printf("Out of space!");
    else
    {
        p->info = x;
        p->link = NULL;
        if (plqu->f == NULL)
            plqu->f = p;
        else
            plqu->r->link = p;
        plqu->r = p;
    }
}

PBinTreeNode DeleteQ(PLinkQueue plqu)
{
    PNode p;
    if (plqu->f == NULL)
    {
        printf("Empty queue.\n");
        return NULL;
    }
    else
    {
        p = plqu->f;
        plqu->f = p->link;
        return p->info;
    }
}

int IsEmpty(PLinkQueue plqu)
{
    return (plqu->f == NULL);
}

int LevelOrder(PBinTreeNode t)
{
    PLinkQueue q;
    PBinTreeNode bt;
    if (!t)
        return 0; // 若是空树直接返回
    q = CreatQ(); // 创建空队列
    AddQ(q, t);
    while (!IsEmpty(q))
    {
        bt = DeleteQ(q);
        printf("% c", bt->info); // 访问取出队列的结点
        if (bt->llink)
            AddQ(q, bt->llink);
        if (bt->rlink)
            AddQ(q, bt->rlink);
    }
    return 1;
} //LevelOrder

int main()
{
    PBinTreeNode tree = createBinTree();
    LevelOrder(tree);
    return 1;
}
