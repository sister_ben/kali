#include "btree.h"
#include "queue.c"
#include <stdio.h>
#include<stdlib.h>

PLinkQueue CreatQ()
{
    PLinkQueue plqu = (PLinkQueue)malloc(sizeof(struct LinkQueue));
    if (plqu != NULL)
    {
        plqu->f = NULL;
        plqu->r = NULL;
    }
    else
        printf("Out of space!!\n");
    return plqu;
};

int IsEmpty(PLinkQueue plqu)
{
    return (plqu->f == NULL);
}

PBinTreeNode DeleteQ(PLinkQueue plqu)
{
    PNode p;
    if (plqu->f == NULL)
    {
        printf("Empty queue.\n");
        return NULL;
    }
    else
    {
        p = plqu->f;
        plqu->f = p->link;
        return p->info;
    }
}

void AddQ(PLinkQueue plqu, PBinTreeNode x)
{
    PNode p;
    p = (PNode)malloc(sizeof(struct Node));
    if (p == NULL)
        printf("Out of space!");
    else
    {
        p->info = x;
        p->link = NULL;
        if (plqu->f == NULL)
            plqu->f = p;
        else
            plqu->r->link = p;
        plqu->r = p;
    }
}