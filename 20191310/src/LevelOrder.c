#include "btree.h"
#include "queue.h"
#include <stdio.h>

int LevelOrder(PBinTreeNode t)
{
    PLinkQueue q;
    PBinTreeNode bt;
    if (!t)
        return 0; // 若是空树直接返回
    q = CreatQ(); // 创建空队列
    AddQ(q, t);
    while (!IsEmpty(q))
    {
        bt = DeleteQ(q);
        printf("% c", bt->info); // 访问取出队列的结点
        if (bt->llink)
            AddQ(q, bt->llink);
        if (bt->rlink)
            AddQ(q, bt->rlink);
    }
    return 1;
} //LevelOrder