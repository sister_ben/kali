#include <stdio.h>
#include <string.h>

int verify(char *password){
    int verify = 0;
    int flag = 1;
    char PASSWORD[8] = {'1','2','3','4','5','6','7','8'};
    char buffer[8] = {0};
    strcpy(buffer, password);
    for(int i = 0; i < 8; i++){
        if(buffer[i] != PASSWORD[i]){
            flag = 0;
            break;
        }
    }
    if(flag == 1)
        verify = 1;
    return verify;
}